
var searchMovie = (title) => {
    $.ajax({
        type: "GET",
        url: `http://www.omdbapi.com/?t=${title}&r=json&plot=full&apikey=ee3c220f`,
        beforeSend: function() {
            $('.movie-details').empty();
            $('.movie-details').append(`
                <h1 class="text-center">SEARCHING...</h1>
            `);
        },
        success: function(data) {
            if(data.Response == "False")
            {
                $('.movie-details').empty();
                $('.movie-details').append(`
                    <h1 class="text-center">NO MOVIE FOUND WITH THAT TITLE</h1>
                `);
            }
            else
            {
                $('.movie-details').empty();

                $('.movie-details').append(`
                    <div class="col-2"></div>
                    <div class="col-2">
                        <img id="poster" class="posterImg" src="${data.Poster}" />
                    </div>
                    <div class="col-6">
                        <h2 class="title">${data.Title}</h2>
                        <hr/>
                        <ul class="details margin-top-5">
                            <li>${data.Rated}</li>
                            <li>${data.Runtime}</li>
                            <li>${data.Genre}</li>
                            <li>${data.Released}</li>
                        </ul>
                        <br/>
                        <p class="margin-top-10"><span class="director">Director</span>: ${data.Director}</p>
                        <p class="margin-top-5"><span class="writer">Writer</span>: ${data.Writer}</p>
                        <p class="margin-top-5"><span class="actors">Actors</span>: ${data.Actors}</p>
                        <p class="margin-top-5"><span class="plot">Plot</span>:</p>
                        <p class="">${data.Plot}</p>
                    </div>
                    <div class="col-2"></div>
                `)
            }
        }
    })
}

$('#search').submit((event) => {
    let title = $('.input-box').val();
    searchMovie(title)
    event.preventDefault();
})